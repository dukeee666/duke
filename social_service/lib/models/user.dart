
class User {
  int userId;
  String firstName;
  String secondName;
  String username;
  String mobile;
  String email;
  String address;
  String avatarUrl;
  String description;
  Gender gender;
  DateTime birthDateUTC;

  User(
    {
    this.userId,
    this.firstName,
    this.secondName,
    this.mobile,
    this.email,
    this.address,
    this.username,
    this.gender,
    this.birthDateUTC,
    this.avatarUrl,
    this.description
    });
}
enum Gender {  
  male,
  female,
  non_binary,
  other
}