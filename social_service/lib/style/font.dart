const int largeScreenTitleFontSize = 25;
const int largeScreenTextBodyFontSize = 20;
const int largeScreenTextBodySmallFontSize = 15;
const int phoneScreenTitleFontSize = 50;
const int phoneScreenTextBodyFontSize = 40;
const int phoneScreenTextBodySmallFontSize = 30;
const int largeScreenTextFieldFontSize = 30;
const int phoneScreenTextFieldFontSize = 40;