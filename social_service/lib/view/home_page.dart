import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:social_service/utils/appBar.dart';
import 'package:flutter_image_slideshow/flutter_image_slideshow.dart';
import 'package:social_service/style/color.dart';
import 'package:social_service/helper/screenHelper.dart';
import 'package:social_service/helper/sizeHelper.dart';
import 'package:flutter_screenutil/screenutil.dart';

class HomePage extends StatefulWidget {

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final appbar = PreferredSize(
          preferredSize: Size.fromHeight(60.0),
          child: CustomAppBar.getAppBar("HOME", false)
      );
  final List<String> hotSearchWords = ["水管工","配钥匙",'油漆',"搬家","地板","清洁","换汇"];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar : appbar,
      body: Container(
        child: Column(
          children: <Widget>[
            Container(
              height: (MediaQuery.of(context).size.height-appbar.preferredSize.height)*0.3,
              child: 
                Center(
                  child: sliderShow(context),
                ),
              padding: EdgeInsets.fromLTRB(20, 30, 20, 20),
            ), 
            searchItemBar(context),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  child: 
                    Text(
                    "Top Search",
                    style: TextStyle(
                      color: Color.fromRGBO(0, 0, 0, 0.35),
                      fontSize: 12
                      ),
                    ),
                  padding: EdgeInsets.fromLTRB(20, 0, 20, 15),
                )
              ],
            ),
            hotWordsBar(context)
          ],
        )
      )
    );
  }
  Widget sliderShow(BuildContext context){
    return ImageSlideshow(
          /// Width of the [ImageSlideshow].
          width: double.infinity,
          /// Height of the [ImageSlideshow].
          height: 170,
          /// The page to show when first creating the [ImageSlideshow].
          initialPage: 0,
          /// The color to paint the indicator.
          indicatorColor: Color.fromRGBO(19, 207, 15, 1),
          /// The color to paint behind th indicator.
          indicatorBackgroundColor: Colors.white,
          children: [
            Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadiusDirectional.circular(20)),
              clipBehavior: Clip.antiAlias,
              child: Image.asset(
                'assets/images/clean.png',
                width: double.maxFinite,
                fit:BoxFit.cover
              ),
            ),
            Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadiusDirectional.circular(20)),
              clipBehavior: Clip.antiAlias,
              child: Image.asset(
                'assets/images/domain.png',
                width: double.maxFinite,
                fit:BoxFit.cover
              ),
            ),
            Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadiusDirectional.circular(20)),
              clipBehavior: Clip.antiAlias,
              child: Image.asset(
                'assets/images/moveHouse.png',
                width: double.maxFinite,
                fit:BoxFit.cover
              ),
            ),
          ],
          autoPlayInterval: 3000,
    );
  }
  Widget searchItemBar(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: 
          Container(
            margin: EdgeInsets.fromLTRB(15, 0, 15, 15),
            child: RawMaterialButton(
              elevation: 3,
              constraints: BoxConstraints(minHeight: (MediaQuery.of(context).size.height-appbar.preferredSize.height)*0.06),
              onPressed: () {},
              fillColor: Colors.white,
              child: 
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                    child: Icon(
                        Icons.search,
                        color: appThemeColor, size: 30
                        ),
                  ),
                  Text(
                      "Search for jobs, Services :)",
                      style:  TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 0.35),
                        fontSize: 15
                        )
                      ),
                  const SizedBox(width: 32.0),
                ],
              ),
              shape: RoundedRectangleBorder(
                borderRadius:BorderRadius.circular(20),
                ),
            ),
          )
        ),
    ]
    );
  }
  Widget hotWordsBar(BuildContext context) {
    return Container(
      child: Wrap(
        spacing: 10,
        runSpacing: 10,
        children: hotSearches(),
      )
    );
  }

  List<Widget> hotSearches() =>
    List.generate(hotSearchWords.length, (index) {
      return Container(
          constraints:
              BoxConstraints(maxWidth:80,maxHeight: 20),
          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: RawMaterialButton(
            elevation: 0,
            onPressed: (){},
            fillColor: Color.fromRGBO(233, 233, 233, 1),
            child: Text(
              hotSearchWords[index],
              style:  TextStyle(
                color: Colors.black,
                fontSize: 10
                )
              )
            ),
          );
      });
  }