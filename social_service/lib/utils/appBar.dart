import 'package:flutter/material.dart';

class CustomAppBar{
  static getAppBar(String title, bool showProfile,
      {dynamic argument,
      Function callBack,
      BuildContext context}){
    return AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        centerTitle: true,
        elevation: 5,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Flexible(
              flex: 3,
              child:
              Text("$title",
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 24,
                  fontWeight: FontWeight.w400
                ),
              ),
            ),
            Flexible(
              flex: 2,
              child: showProfile
                    ? Container()
                    : loginButton(context,"Login")
            )
          ]
        )
      );
    }
    static Widget loginButton(BuildContext context,String content) {
      return Container(
        child: 
          RawMaterialButton(
            constraints: BoxConstraints.tight(Size(50, 50)),
            onPressed: () {},
            fillColor: Color.fromRGBO(233, 233, 233, 1),
            child: Text(
              "$content",
              style:  TextStyle(
                color: Color.fromRGBO(19, 207, 15, 1),
                fontSize: 15
                )
              ),
            padding: EdgeInsets.all(5.0),
            shape: CircleBorder(),
          )
      );
    }
}