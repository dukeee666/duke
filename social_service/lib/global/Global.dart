import 'dart:io';
import 'package:dio/dio.dart';
// Dio 封装 (单例)
class Global{
  static Global _instance;
  Dio dio;
  
  static Global getInstance(){
    if(_instance==null){_instance = new Global();}
    return _instance;
  }
  Global(){
    dio = new Dio();
    dio.options = BaseOptions(
      // URL 头部, e.g "https://api..."
      baseUrl: "",
      // 判断连接是否超时, >5s, 连接超时
      connectTimeout: 5000,
      // 判断发送请求是否超时, >5s, 发送超时
      sendTimeout: 5000,
      // 判断接受是否超时, >5s, 接受超时
      receiveTimeout: 5000,
      headers: {
        "token": "31313123"
      },
      contentType: Headers.jsonContentType,
      responseType: ResponseType.json
    );
    // 监听
    dio.interceptors.add(InterceptorsWrapper(
      onRequest: (options){
        print("请求");
      },
      onResponse: (e){
        print("返回");
      },
      onError: (e){
        print("错误");
      }
    ));
  }
}